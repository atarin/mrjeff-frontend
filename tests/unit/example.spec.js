import { createLocalVue, mount } from '@vue/test-utils';
import VueLodash from 'vue-lodash';
import VueRouter from 'vue-router';
import Button from '@/components/Button';

const localVue = createLocalVue();
localVue.use(VueLodash);
localVue.use(VueRouter);
const $router = new VueRouter();

localVue.use(VueLodash);

describe('OrderRow.vue', () => {
  const mockData = {
    order: {
      code: '0',
      orderDate: '1/1/1970',
      logistics: { code: '0' },
    },
  };
  const $route = {
    name: 'OrderDetail',
    path: '/orders/detail',
  };
  const wrapper = mount(Button, {
    propsData: { params: {} },
    mocks: {
      $route,
      $router,
    },
  });
  it('Check order row', () => {
    const defaultData = wrapper.vm.$props;
    expect(defaultData.params).toEqual({});
  });
  it('Check routing', () => {
    wrapper.find('#button').trigger('click');
    expect(wrapper.vm.$route.path).toEqual('/orders/detail');
  });
});
