import Vue from 'vue';
import Router from 'vue-router';
import OrderDetail from './components/OrderDetail';
import OrderList from './components/OrdersList';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/orders',
      name: 'OrdersList',
      component: OrderList,
    },
    {
      path: '/orders/detail',
      name: 'OrderDetail',
      props: true,
      component: OrderDetail,
    },

  ],
});
