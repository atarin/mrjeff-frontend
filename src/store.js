import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    current:0
  },
  mutations: {
    plus(state){
      state.current++;
    },
    minus(state){
      state.current--;
    }
  },
  actions: {

  },
  getters:{
    step:state=>state.current,
  }
})
